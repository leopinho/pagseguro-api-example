﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pagseguro_Angularjs.Models
{
    [Table("Products")]
    public class Product
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Favor informar a descrição do produto.")]
        public string description { get; set; }

        public string imageUrl { get; set; }
        public double purchasePrice { get; set; }
        public double salePrice { get; set; }
        public double stockQuantity { get; set; }
        public DateTime lastUpdate { get; set; }
    }
}
