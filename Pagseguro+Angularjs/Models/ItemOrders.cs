﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pagseguro_Angularjs.Models
{
    public class ItemOrders
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int quantity { get; set; }

        public decimal unitaryValue { get; set; }

        public double subTotal { get; set; }

        [Required]
        [Column("id_status")]
        public int codStatus { get; set; }

        [ForeignKey("codStatus")]
        public virtual Status status { get; set; }

        [Required]
        [Column("id_product")]
        public int idProduct { get; set; }

        [ForeignKey("idProduct")]
        public virtual Product product { get; set; }

        public virtual Orders orders { get; set; }

    }
}
