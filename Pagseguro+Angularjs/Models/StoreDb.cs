﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Pagseguro_Angularjs.Models
{
    public class StoreDb : DbContext
    {
        public DbSet<Customer> customer { get; set; }
        public DbSet<Product> product { get; set; }
        public DbSet<Orders> orders { get; set; }
        public DbSet<ItemOrders> itemOrders { get; set; }

        public StoreDb() : base("StoreDb"){}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}