﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pagseguro_Angularjs.Models
{
    [Table("Status")]
    public class Status
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(50)]
        [Column("type")]
        [Required(ErrorMessage = "Descrição para o tipo do status é obrigatório.")]
        public string description { get; set; }
    }
}
