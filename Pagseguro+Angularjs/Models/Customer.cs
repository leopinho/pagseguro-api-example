﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Pagseguro_Angularjs.Models
{
    [Table("Customers")]
    public class Customer
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Informar o nome do cliente.")]
        public string name { get; set; }
        
        [StringLength(100)]
        [Required(ErrorMessage = "Informar o email do cliente.")]
        public string email { get; set; }

        public bool active { get; set; }

        [StringLength(11)]
        public string cpf { get; set; }

        public string street { get; set; }
        public string reference { get; set; }
        public string number { get; set; }
        public string district { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string cep { get; set; }
        public string ddd { get; set; }
        public string telephone { get; set; }

        public DateTime lastUpdate { get; set; }
    }
}