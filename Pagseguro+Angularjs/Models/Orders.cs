﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pagseguro_Angularjs.Models
{
    [Table("Orders")]
    public class Orders
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public double totalValue { get; set; }

        public int quantityItems { get; set; }

        [Required]
        [Column("id_costumer")]
        public int idCustomer { get; set; }

        [ForeignKey("idCustomer")]
        public virtual Customer customer { get; set; }

        [Required]
        [Column("id_status")]
        public int codStatus { get; set; }

        [ForeignKey("codStatus")]
        public virtual Status status { get; set; }

        public virtual List<ItemOrders> listOrders { get; set; }

        public DateTime created { get; set; }
        public DateTime lastUpdate { get; set; }

    }
}
