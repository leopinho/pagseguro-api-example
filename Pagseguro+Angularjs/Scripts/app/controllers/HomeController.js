﻿app.controller('HomeController', ['$scope', 'CustomersService', 'ProductsService', 'PaymentsService', function($scope, CustomersService, ProductsService, PaymentsService) {

    $scope.customers = [];
    $scope.customer = null;
    $scope.selectedCustomer = 1;
    $scope.products = [];
    $scope.product = null;
    $scope.selectedProducts = [];
    $scope.action = null;

    $scope.init = function() {
        $scope.GetAllCustomers();
        $scope.GetAllProducts();
    };

    /**
     * Customers
     * Add, Edit and Remove 
     */

     $scope.GetAllCustomers = function() {

        CustomersService.GetAllCustomers()
        .success(function(result) {
            $scope.customers = result;
        })
        .error(function(error) {
            toastr["error"]("Erro ao listar compradores", "Loja");
        });

    };

    $scope.CustomerStatus = function(status) {
        return status ? "ativo" : "inativo";
    };

    $scope.AddCustomer = function() {

        $('#modalCustomer').modal('show');

        $scope.action = "Adicionar";
        $scope.customer = {
            id: 0,
            name: null
        };

    };

    $scope.EditCustomer = function(customer) {

        $('#modalCustomer').modal('show');

        $scope.action = "Editar";
        $scope.customer = customer;

    };

    $scope.RemoveCustomer = function(customer) {

        CustomersService.DeleteCustomers(customer.id)
        .success(function(result) {
            toastr["success"]("Comprador removido com sucesso!", "Loja");
            $scope.GetAllCustomers();
        })
        .error(function(error) {
            toastr["error"]("Erro ao remover comprador", "Loja");
        });

    };

    $scope.SetSelectedCustomer = function(customer) {
        $scope.selectedCustomer = customer;
    };

    $scope.SaveCustomer = function() {

        if ($scope.customer.id == 0) {

            //add
            CustomersService.AddCustomers($scope.customer)
            .success(function(result) {
                toastr["success"]("Comprador adicionado com sucesso!", "Loja");
                $scope.GetAllCustomers();
                $('#modalCustomer').modal('hide');
            })
            .error(function(error) {
                toastr["error"]("Erro ao adicionar comprador", "Loja");
            });

        } else {

            //edit
            CustomersService.UpdateCustomers($scope.customer)
            .success(function(result) {
                toastr["success"]("Comprador editado com sucesso!", "Loja");
                $scope.GetAllCustomers();
                $('#modalCustomer').modal('hide');
            })
            .error(function(error) {
                toastr["error"]("Erro ao editar comprador", "Loja");
            });

        }

    };

    $scope.CancelAddEditCustomer = function() {
        $scope.$broadcast("CloseModal");
    };

    /**
    * Products
    * Add, Edit and Remove 
    */

    $scope.GetAllProducts = function() {

        ProductsService.GetAllProducts()
        .success(function(result) {
            $scope.products = result;
        })
        .error(function(error) {
            toastr["error"]("Erro ao listar produtos", "Loja");
        });

    };

    $scope.AddProduct = function() {

        $('#modalProduct').modal('show');

        $scope.action = "Adicionar";
        $scope.product = {
            id: 0,
            description: null
        };

    };

    $scope.EditProduct = function(product) {

        $('#modalProduct').modal('show');

        $scope.action = "Editar";
        $scope.product = product;

    };

    $scope.RemoveProduct = function(product) {

        ProductsService.DeleteProducts(product.id)
        .success(function(result) {
            toastr["success"]("Produto removido com sucesso!", "Loja");
            $scope.GetAllProducts();
        })
        .error(function(error) {
            toastr["error"]("Erro ao remover produto", "Loja");
        });

    };

    $scope.SaveProduct = function() {

        if ($scope.product.id == 0) {

            //add
            ProductsService.AddProducts($scope.product)
            .success(function(result) {
                toastr["success"]("Produto adicionado com sucesso!", "Loja");
                $scope.GetAllProducts();
                $('#modalProduct').modal('hide');
            })
            .error(function(error) {
                toastr["error"]("Erro ao adicionar o produto", "Loja");
            });

        } else {

            //edit
            ProductsService.UpdateProducts($scope.product)
            .success(function(result) {
                toastr["success"]("Produto editado com sucesso!", "Loja");
                $scope.GetAllProducts();
                $('#modalProduct').modal('hide');
            })
            .error(function(error) {
                toastr["error"]("Erro ao editar produto", "Loja");
            });

        }

    };

    $scope.isSelected = function(product) {
        return $scope.selectedProducts.indexOf(product.id) >= 0;
    };

    $scope.changeSelectedProduct = function($event, product) {

        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');

        if (action === 'add' && $scope.selectedProducts.indexOf(product.id) === -1) {
            $scope.selectedProducts.push(product.id);
        }

        if (action === 'remove' && $scope.selectedProducts.indexOf(product.id) !== -1) {
            $scope.selectedProducts.splice($scope.selectedProducts.indexOf(product.id), 1);
        }

    };

    $scope.CancelAddEditProduct = function() {
        $scope.$broadcast("CloseModal");
    };

    /**
     * Payment
     */

     $scope.sendPayment = function() {

        var orders = GetOrders();
        
        //init modal
        $('#finishModal').modal('show');

        PaymentsService.SetPayment(orders)
        .success(function(result) {

                //modal
                var iframeUrl = result;
                document.getElementById('iframeEnd').src = iframeUrl;
                //toastr["success"]("Compra aprovada!", "Loja");

            })
        .error(function(error) {
            $('#finishModal').modal('hide');
            toastr["error"]("Erro ao finalizar compra.", "Loja");
        });

    };

    GetOrders = function() {

        var result = {
            id: 0,
            totalValue: GetTotalValueOfItems(),
            quantityItems: GetTotalLengthOfItems(),
            idCustomer: $scope.customers[0].id,
            codStatus: 3,
            listOrders: GetItemOrders()
        };

        return result;
    };

    GetItemOrders = function() {

        var result = [];

        angular.forEach($scope.products, function(produto) {

            angular.forEach($scope.selectedProducts, function(p_id, i) {

                if (p_id == produto.id) {

                    var item = {
                        id: 0,
                        quantity: 1,
                        unitaryValue: produto.salePrice,
                        codStatus: 3,
                        idProduct: produto.id,
                    };

                    result.push(item);

                }

            });

        });

        return result;
    };

    GetTotalValueOfItems = function() {

        var result = 0;
        var itemOrders = GetItemOrders();

        angular.forEach(itemOrders, function(item) {
            result += (item.unitaryValue * item.quantity);
        });

        return result;

    };

    GetTotalLengthOfItems = function() {

        var result = 0;
        var itemOrders = GetItemOrders();

        angular.forEach(itemOrders, function(item) {

            result += item.quantity;
        });

        return result;

    };

}])
