﻿app.service('PaymentsService', ['$http', '$location', function($http, $location) {

    var baseUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/api/v1/public/purchase";

    this.SetPayment = function(orders) {
        var response = $http({
            method: "post",
            url: baseUrl + "/setPayment/",
            data: JSON.stringify(orders),
            dataType: "json"
        });

        return response;
    }

}]);
