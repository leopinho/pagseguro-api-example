﻿app.service('ProductsService', ['$http', '$location', function($http, $location) {

    var baseUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/api/v1/public/products";

    this.GetAllProducts = function() {
        return $http.get(baseUrl + "/getAll");
    }

    this.GetProductsById = function(id) {
        return $http.post(baseUrl + "/getById/" + id);
    }

    this.AddProducts = function(product) {
        var response = $http({
            method: "post",
            url: baseUrl + "/add/",
            data: JSON.stringify(product),
            dataType: "json"
        });

        return response;
    }

    this.UpdateProducts = function(product) {
        var response = $http({
            method: "put",
            url: baseUrl + "/update/",
            data: JSON.stringify(product),
            dataType: "json"
        });

        return response;
    }

    this.DeleteProducts = function(id) {
        return $http.delete(baseUrl + "/delete/" + id);
    }

}]);
