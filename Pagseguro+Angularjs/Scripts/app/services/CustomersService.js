﻿app.service('CustomersService', ['$http', '$location', function($http, $location) {

    var baseUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/api/v1/public/customers";

    this.GetAllCustomers = function() {
        return $http.get(baseUrl + "/getAll");
    }

    this.GetCustomersById = function(id) {
        return $http.post(baseUrl + "/getById/" + id);
    }

    this.AddCustomers = function(customer) {
        var response = $http({
            method: "post",
            url: baseUrl + "/add/",
            data: JSON.stringify(customer),
            dataType: "json"
        });

        return response;
    }

    this.UpdateCustomers = function(customer) {
        var response = $http({
            method: "put",
            url: baseUrl + "/update/",
            data: JSON.stringify(customer),
            dataType: "json"
        });

        return response;
    }

    this.DeleteCustomers = function(id) {
        return $http.delete(baseUrl + "/delete/" + id);
    }

}])
