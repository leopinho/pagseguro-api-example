﻿using System.Web;
using System.Web.Optimization;

namespace Pagseguro_Angularjs
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(                    
                    "~/Scripts/angular.js",
                    "~/Scripts/app/App.js",
                    "~/Scripts/app/services/CustomersService.js",
                    "~/Scripts/app/services/ProductsService.js",
                    "~/Scripts/app/services/PaymentsService.js",
                    "~/Scripts/app/controllers/HomeController.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                   "~/Scripts/toastr.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/toastr.css",
                      "~/Content/main.css"));
        }
    }
}
