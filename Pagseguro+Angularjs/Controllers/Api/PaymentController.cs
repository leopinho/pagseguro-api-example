﻿using Pagseguro_Angularjs.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Uol.PagSeguro.Constants;
using Uol.PagSeguro.Domain;
using Uol.PagSeguro.Exception;
using Uol.PagSeguro.Resources;

namespace Pagseguro_Angularjs.Controllers.Api
{
    [RoutePrefix("api/v1/public")]
    public class PaymentController : ApiController
    {
        private readonly StoreDb _db = new StoreDb();

        private AccountCredentials _credential;
        private PaymentRequest _payment;
        private Uri _paymentRedirectUri;

        private bool _isSandbox = true;
        private string _redirectUrl = "http://leonardopinho.azurewebsites.net/";

        [HttpPost]
        [Route("purchase/setPayment")]
        public HttpResponseMessage SetPayment(Orders orders)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //add orders
                    orders.created = DateTime.Now;
                    orders.lastUpdate = orders.created;

                    _db.orders.Add(orders);
                    _db.SaveChanges();

                    // pagseguro API 
                    EnvironmentConfiguration.ChangeEnvironment(_isSandbox);

                    _payment = new PaymentRequest();

                    _credential = PagSeguroConfiguration.Credentials(_isSandbox);

                    _payment.Currency = Currency.Brl;

                    foreach (ItemOrders itemOrders in orders.listOrders)
                    {
                        //Get product
                        Product product = _db.product.Find(itemOrders.idProduct);

                        _payment.Items.Add(
                            new Item(product.id.ToString(),
                                product.description,
                                itemOrders.quantity,
                                itemOrders.unitaryValue)
                        );
                    }

                    _payment.Reference = orders.id.ToString();

                    _payment.Shipping = new Shipping();
                    _payment.Shipping.ShippingType = ShippingType.Sedex;

                    _payment.Shipping.Cost = 10.00m;

                    //Get customer
                    Customer customer = _db.customer.Find(orders.idCustomer);

                    _payment.Shipping.Address = new Address(
                         customer.country,
                         customer.state,
                         customer.city,
                         customer.district,
                         customer.cep,
                         customer.street,
                         customer.number,
                         customer.reference
                     );

                    _payment.Sender = new Sender(
                        customer.name,
                        customer.email,
                        new Phone(customer.ddd, customer.telephone)
                     );

                    _payment.RedirectUri = new Uri(_redirectUrl);

                    if (!string.IsNullOrEmpty(orders.customer.cpf))
                    {
                        SenderDocument senderCPF = new SenderDocument(Documents.GetDocumentByType("CPF"), orders.customer.cpf);
                        _payment.Sender.Documents.Add(senderCPF);
                    }

                    _paymentRedirectUri = _payment.Register(_credential);

                    return Request.CreateResponse(HttpStatusCode.OK, _paymentRedirectUri);

                }
                catch (PagSeguroServiceException ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Errors.ToString());
                }

            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);

        }

    }
}
