﻿using Pagseguro_Angularjs.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pagseguro_Angularjs.Controllers.Api
{
    [RoutePrefix("api/v1/public")]
    public class CustomersController : ApiController
    {
        private readonly StoreDb _db = new StoreDb();

        [HttpGet]
        [Route("customers/getAll")]
        public IQueryable<Customer> getAllCustomers()
        {
            return _db.customer;
        }

        [HttpPost]
        [Route("customers/getById/{id:int}")]
        public HttpResponseMessage getCustomersById(int? id)
        {
            if (id == null)
            Request.CreateResponse(HttpStatusCode.BadRequest, "O id nao pode ser nulo");

            Customer customer = _db.customer.Find(id);

            return Request.CreateResponse(HttpStatusCode.OK, customer);
        }

        [HttpPut]
        [Route("customers/update")]
        public HttpResponseMessage updateCustomer(Customer customer)
        {

            if (ModelState.IsValid)
            {
                _db.Entry(customer).State = EntityState.Modified;
                _db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK);
            }


            return Request.CreateResponse(HttpStatusCode.BadRequest);

        }

        [HttpPost]
        [Route("customers/add")]
        public HttpResponseMessage addCustomer(Customer customer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    customer.lastUpdate = DateTime.Now;

                    _db.customer.Add(customer);
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                

                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);

        }

        [HttpDelete]
        [Route("customers/delete/{id:int}")]
        public HttpResponseMessage deleteCustomer(int? id)
        {
            if (id == null)
            Request.CreateResponse(HttpStatusCode.BadRequest, "O id nao pode ser nulo");

            Customer customer = _db.customer.Find(id);

            _db.customer.Remove(customer);
            _db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);

        }

        protected override void Dispose(bool disponsing)
        {
            if (disponsing)
            {
                _db.Dispose();
            }

            base.Dispose(disponsing);
        }
    }
}
