﻿using Pagseguro_Angularjs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pagseguro_Angularjs.Controllers.Api
{
    [RoutePrefix("api/v1/public")]
    public class ProductsController : ApiController
    {
        private readonly StoreDb _db = new StoreDb();

        [HttpGet]
        [Route("products/getAll")]
        public IQueryable getAllProducts()
        {
            return _db.product;
        }

        [HttpPost]
        [Route("products/getById/{id:int}")]
        public HttpResponseMessage getProductsById(int? id)
        {
            if (id == null)
                Request.CreateResponse(HttpStatusCode.BadRequest, "O id nao pode ser nulo");

            Product product = _db.product.Find(id);

            return Request.CreateResponse(HttpStatusCode.OK, product);
        }

        [HttpPut]
        [Route("products/update")]
        public HttpResponseMessage updateCustomer(Product product)
        {

            if (ModelState.IsValid)
            {
                _db.Entry(product).State = EntityState.Modified;
                _db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK);
            }


            return Request.CreateResponse(HttpStatusCode.BadRequest);

        }

        [HttpPost]
        [Route("products/add")]
        public HttpResponseMessage addCustomer(Product product)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    product.lastUpdate = DateTime.Now;

                    _db.product.Add(product);
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);

        }

        [HttpDelete]
        [Route("products/delete/{id:int}")]
        public HttpResponseMessage deleteCustomer(int? id)
        {
            if (id == null)
                Request.CreateResponse(HttpStatusCode.BadRequest, "O id nao pode ser nulo");

            Product product = _db.product.Find(id);

            _db.product.Remove(product);
            _db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);

        }

        protected override void Dispose(bool disponsing)
        {
            if (disponsing)
            {
                _db.Dispose();
            }

            base.Dispose(disponsing);
        }
    }
}
